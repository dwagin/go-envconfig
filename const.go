package envconfig

const (
	// TagEnv name of the environment variable or a list of names.
	TagEnv = "env"

	// TagEnvTimeLayout value parsing layout (for types like time.Time).
	TagEnvTimeLayout = "env-time-layout"

	// EnvSeparator prefix separator.
	EnvSeparator = "_"
)
