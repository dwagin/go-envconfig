package envconfig_test

import (
	"errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"go.openly.dev/pointy"

	"gitlab.com/dwagin/go-envconfig"
)

//nolint:govet // tests
type (
	UnmarshalText uint32

	UnmarshalJSON uint32

	AllTypes struct {
		Boolean  bool          `env:"BOOLEAN" yaml:"boolean"`
		Int      int           `env:"INT" yaml:"int"`
		Int64    int64         `env:"INT64" yaml:"int64"`
		Unsigned uint64        `env:"UNSIGNED" yaml:"unsigned"`
		Float    float64       `env:"FLOAT" yaml:"float"`
		String   string        `env:"STRING" yaml:"string"`
		UText    UnmarshalText `env:"UNMARSHAL_TEXT" yaml:"unmarshal_text"`
		UJson    UnmarshalJSON `env:"UNMARSHAL_JSON" yaml:"unmarshal_json"`
		Duration time.Duration `env:"DURATION" yaml:"duration"`
		Time     time.Time     `env:"TIME" yaml:"time"`
		// Location depends on the system, so we test it with time.UTC
		Location        *time.Location    `env:"LOCATION" yaml:"location"`
		URL             url.URL           `env:"URL" yaml:"url"`
		ArrayInt        []int             `env:"ARRAY_INT" yaml:"array_int"`
		ArrayString     []string          `env:"ARRAY_STRING" yaml:"array_string"`
		MapStringInt    map[string]int    `env:"MAP_STRING_INT" yaml:"map_string_int"`
		MapStringString map[string]string `env:"MAP_STRING_STRING" yaml:"map_string_string"`
	}

	AllPointerTypes struct {
		Boolean  *bool          `env:"BOOLEAN"`
		Int      *int           `env:"INT"`
		Int64    *int64         `env:"INT64"`
		Unsigned *uint64        `env:"UNSIGNED"`
		Float    *float64       `env:"FLOAT"`
		String   *string        `env:"STRING"`
		UText    *UnmarshalText `env:"UNMARSHAL_TEXT"`
		UJson    *UnmarshalJSON `env:"UNMARSHAL_JSON"`
		Duration *time.Duration `env:"DURATION"`
		Time     *time.Time     `env:"TIME"`
		// Location depends on the system, so we test it with time.UTC
		Location        **time.Location    `env:"LOCATION"`
		URL             *url.URL           `env:"URL"`
		ArrayInt        *[]int             `env:"ARRAY_INT"`
		ArrayString     *[]string          `env:"ARRAY_STRING"`
		MapStringInt    *map[string]int    `env:"MAP_STRING_INT"`
		MapStringString *map[string]string `env:"MAP_STRING_STRING"`
	}

	PrivateFieldsValid struct {
		//nolint:unused // test case
		boolean bool
	}

	PrivateFieldsInvalid struct {
		//nolint:unused // test case
		boolean bool `env:"BOOLEAN"`
	}

	Structure struct {
		Nested AllTypes `env:"NESTED"`
	}

	StructureEmptyTag struct {
		Nested AllTypes
	}

	PointerStructure struct {
		Nested *AllTypes `env:"NESTED"`
	}

	PointerStructureEmptyTag struct {
		Nested *AllTypes
	}

	PointerPointerStructure struct {
		Nested **AllTypes `env:"NESTED"`
	}

	StructurePrivateFieldsInvalid struct {
		Nested PrivateFieldsInvalid
	}

	PointerStructurePrivateFieldsInvalid struct {
		Nested *PrivateFieldsInvalid
	}

	UnsupportedType struct {
		Func func() `env:"UNSUPPORTED"`
	}
)

const (
	ZeroUnmarshalText UnmarshalText = iota
	OneUnmarshalText
	TwoUnmarshalText
)

const (
	ZeroUnmarshalJSON UnmarshalJSON = iota
	OneUnmarshalJSON
	TwoUnmarshalJSON
)

func (v *UnmarshalText) UnmarshalText(text []byte) error {
	switch strings.ToLower(string(text)) {
	case "zero":
		*v = ZeroUnmarshalText
	case "one":
		*v = OneUnmarshalText
	case "two":
		*v = TwoUnmarshalText
	default:
		return fmt.Errorf("unknown unmarshal text: %q", string(text))
	}

	return nil
}

func (v *UnmarshalJSON) UnmarshalJSON(text []byte) error {
	switch strings.ToLower(string(text)) {
	case "zero":
		*v = ZeroUnmarshalJSON
	case "one":
		*v = OneUnmarshalJSON
	case "two":
		*v = TwoUnmarshalJSON
	default:
		return fmt.Errorf("unknown unmarshal json: %q", string(text))
	}

	return nil
}

func parseDuration(str string) time.Duration {
	val, err := time.ParseDuration(str)
	if err != nil {
		panic(err)
	}

	return val
}

func parseTime(str, layout string) time.Time {
	val, err := time.Parse(layout, str)
	if err != nil {
		panic(err)
	}

	return val
}

func parseURL(str string) url.URL {
	val, err := url.Parse(str)
	if err != nil {
		panic(err)
	}

	return *val
}

//nolint:govet // tests
var configTestCases = []struct {
	Name         string
	FileName     string
	WantConfig   any
	ResultConfig any
	ResultError  error
}{
	{
		Name:         "Non existent config file",
		FileName:     "test/non_existent_file.yml",
		ResultConfig: nil,
		ResultError:  errors.New("no such file or directory"),
	},

	{
		Name:         "Unknown field",
		FileName:     "test/unknown_field.yml",
		ResultConfig: &AllTypes{},
		ResultError:  errors.New("field unknown_field not found"),
	},

	{
		Name:     "Valid",
		FileName: "test/valid.yml",
		WantConfig: &AllTypes{
			Boolean:     true,
			Int:         -4,
			Int64:       -5,
			Unsigned:    5,
			Float:       5.5,
			String:      "test",
			UText:       OneUnmarshalText,
			Duration:    parseDuration("1h5m10s"),
			Time:        parseTime("2012-04-23T18:25:43.511Z", time.RFC3339),
			ArrayInt:    []int{1, 2, 3},
			ArrayString: []string{"a", "b", "c"},
			MapStringInt: map[string]int{
				"a": 1,
				"b": 2,
				"c": 3,
			},
			MapStringString: map[string]string{
				"a": "x",
				"b": "y",
				"c": "z",
			},
		},
		ResultConfig: &AllTypes{},
		ResultError:  nil,
	},
}

func TestReadConfig(t *testing.T) {
	for _, test := range configTestCases {
		t.Run(test.Name, func(t *testing.T) {
			err := envconfig.ReadConfig(test.ResultConfig, test.FileName)
			if test.ResultError != nil {
				require.Error(t, err)
				require.ErrorContains(t, err, test.ResultError.Error())
			} else {
				require.NoError(t, err)
				require.Equal(t, test.WantConfig, test.ResultConfig)
			}
		})
	}
}

//nolint:govet // tests
var envTestCases = []struct {
	Name         string
	Env          map[string]string
	WantConfig   any
	ResultConfig any
	ResultError  error
}{
	{
		Name: "All types",
		Env: map[string]string{
			"TEST_BOOLEAN":        "true",
			"TEST_INT":            "-4",
			"TEST_INT64":          "-5",
			"TEST_UNSIGNED":       "5",
			"TEST_FLOAT":          "5.5",
			"TEST_STRING":         "test",
			"TEST_UNMARSHAL_TEXT": "one",
			"TEST_UNMARSHAL_JSON": "one",
			"TEST_DURATION":       "1h5m10s",
			"TEST_TIME":           "2012-04-23T18:25:43.511Z",
			"TEST_URL":            "https://test.org",
			// Location depends on the system, so we test it with time.UTC
			"TEST_LOCATION":          "UTC",
			"TEST_ARRAY_INT":         "1, 2, 3",
			"TEST_ARRAY_STRING":      "a, b, c",
			"TEST_MAP_STRING_INT":    "a:1, b:2, c:3",
			"TEST_MAP_STRING_STRING": "a:x, b:y, c:z",
		},
		WantConfig: &AllTypes{
			Boolean:     true,
			Int:         -4,
			Int64:       -5,
			Unsigned:    5,
			Float:       5.5,
			String:      "test",
			UText:       OneUnmarshalText,
			UJson:       OneUnmarshalJSON,
			Duration:    parseDuration("1h5m10s"),
			Time:        parseTime("2012-04-23T18:25:43.511Z", time.RFC3339),
			Location:    time.UTC,
			URL:         parseURL("https://test.org"),
			ArrayInt:    []int{1, 2, 3},
			ArrayString: []string{"a", "b", "c"},
			MapStringInt: map[string]int{
				"a": 1,
				"b": 2,
				"c": 3,
			},
			MapStringString: map[string]string{
				"a": "x",
				"b": "y",
				"c": "z",
			},
		},
		ResultConfig: &AllTypes{},
		ResultError:  nil,
	},

	{
		Name: "All pointer types",
		Env: map[string]string{
			"TEST_BOOLEAN":        "true",
			"TEST_INT":            "-4",
			"TEST_INT64":          "-5",
			"TEST_UNSIGNED":       "5",
			"TEST_FLOAT":          "5.5",
			"TEST_STRING":         "test",
			"TEST_UNMARSHAL_TEXT": "two",
			"TEST_UNMARSHAL_JSON": "two",
			"TEST_DURATION":       "1h5m10s",
			"TEST_TIME":           "2012-04-23T18:25:43.511Z",
			// Location depends on the system, so we test it with time.UTC
			"TEST_LOCATION":          "UTC",
			"TEST_URL":               "https://test.org",
			"TEST_ARRAY_INT":         "1, 2, 3",
			"TEST_ARRAY_STRING":      "a, b, c",
			"TEST_MAP_STRING_INT":    "a:1, b:2, c:3",
			"TEST_MAP_STRING_STRING": "a:x, b:y, c:z",
		},
		WantConfig: &AllPointerTypes{
			Boolean:     pointy.Bool(true),
			Int:         pointy.Int(-4),
			Int64:       pointy.Int64(-5),
			Unsigned:    pointy.Uint64(5),
			Float:       pointy.Float64(5.5),
			String:      pointy.String("test"),
			UText:       pointy.Pointer(TwoUnmarshalText),
			UJson:       pointy.Pointer(TwoUnmarshalJSON),
			Duration:    pointy.Pointer(parseDuration("1h5m10s")),
			Time:        pointy.Pointer(parseTime("2012-04-23T18:25:43.511Z", time.RFC3339)),
			Location:    pointy.Pointer(time.UTC),
			URL:         pointy.Pointer(parseURL("https://test.org")),
			ArrayInt:    pointy.Pointer([]int{1, 2, 3}),
			ArrayString: pointy.Pointer([]string{"a", "b", "c"}),
			MapStringInt: pointy.Pointer(map[string]int{
				"a": 1,
				"b": 2,
				"c": 3,
			}),
			MapStringString: pointy.Pointer(map[string]string{
				"a": "x",
				"b": "y",
				"c": "z",
			}),
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  nil,
	},

	{
		Name: "Invalid boolean",
		Env: map[string]string{
			"TEST_BOOLEAN": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("strconv.ParseBool"),
	},

	{
		Name: "Invalid int",
		Env: map[string]string{
			"TEST_INT": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("strconv.ParseInt"),
	},

	{
		Name: "Invalid int64",
		Env: map[string]string{
			"TEST_INT64": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("strconv.ParseInt"),
	},

	{
		Name: "Invalid time.Duration",
		Env: map[string]string{
			"TEST_DURATION": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("time: invalid duration"),
	},

	{
		Name: "Invalid uint",
		Env: map[string]string{
			"TEST_UNSIGNED": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("strconv.ParseUint"),
	},

	{
		Name: "Invalid float64",
		Env: map[string]string{
			"TEST_FLOAT": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("strconv.ParseFloat"),
	},

	{
		Name: "Invalid time.Time",
		Env: map[string]string{
			"TEST_TIME": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("parsing time"),
	},

	{
		Name: "Invalid time.Location",
		Env: map[string]string{
			"TEST_LOCATION": "bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("unknown time zone"),
	},

	{
		Name: "Invalid url.URL",
		Env: map[string]string{
			"TEST_URL": ":://bla-bla-bla",
		},
		ResultConfig: &AllPointerTypes{},
		ResultError:  errors.New("missing protocol scheme"),
	},

	{
		Name: "Private field (valid)",
		Env: map[string]string{
			"TEST_BOOLEAN": "true",
		},
		WantConfig:   &PrivateFieldsValid{},
		ResultConfig: &PrivateFieldsValid{},
		ResultError:  nil,
	},

	{
		Name: "Private field (invalid)",
		Env: map[string]string{
			"TEST_BOOLEAN": "true",
		},
		ResultConfig: &PrivateFieldsInvalid{},
		ResultError:  errors.New("cannot parse private fields"),
	},

	{
		Name: "Structure",
		Env: map[string]string{
			"TEST_NESTED_BOOLEAN": "true",
		},
		WantConfig: &Structure{
			Nested: AllTypes{
				Boolean: true,
			},
		},
		ResultConfig: &Structure{},
		ResultError:  nil,
	},

	{
		Name: "Structure (empty tag)",
		Env: map[string]string{
			"TEST_BOOLEAN": "true",
		},
		WantConfig: &StructureEmptyTag{
			Nested: AllTypes{
				Boolean: true,
			},
		},
		ResultConfig: &StructureEmptyTag{},
		ResultError:  nil,
	},

	{
		Name: "Pointer to structure",
		Env: map[string]string{
			"TEST_NESTED_BOOLEAN": "true",
		},
		WantConfig: &PointerStructure{
			Nested: pointy.Pointer(AllTypes{
				Boolean: true,
			}),
		},
		ResultConfig: &PointerStructure{},
		ResultError:  nil,
	},

	{
		Name: "Pointer to structure (empty tag)",
		Env: map[string]string{
			"TEST_BOOLEAN": "true",
		},
		WantConfig: &PointerStructureEmptyTag{
			Nested: pointy.Pointer(AllTypes{
				Boolean: true,
			}),
		},
		ResultConfig: &PointerStructureEmptyTag{},
		ResultError:  nil,
	},

	{
		Name: "Pointer to pointer to structure",
		Env: map[string]string{
			"TEST_NESTED_BOOLEAN": "true",
		},
		WantConfig: &PointerPointerStructure{
			Nested: pointy.Pointer(
				pointy.Pointer(AllTypes{
					Boolean: true,
				}),
			),
		},
		ResultConfig: &PointerPointerStructure{
			Nested: pointy.Pointer[*AllTypes](nil),
		},
		ResultError: nil,
	},

	{
		Name:         "Structure with private field (invalid)",
		ResultConfig: &StructurePrivateFieldsInvalid{},
		ResultError:  errors.New("cannot parse private fields"),
	},

	{
		Name:         "Pointer to structure with private field (invalid)",
		ResultConfig: &PointerStructurePrivateFieldsInvalid{},
		ResultError:  errors.New("cannot parse private fields"),
	},

	{
		Name:         "Pointer expected",
		ResultConfig: true,
		ResultError:  errors.New("pointer expected"),
	},

	{
		Name:         "Structure expected",
		ResultConfig: pointy.Bool(true),
		ResultError:  errors.New("structure expected"),
	},

	{
		Name: "Unsupported type",
		Env: map[string]string{
			"TEST_UNSUPPORTED": "bla-bla-bla",
		},
		ResultConfig: &UnsupportedType{},
		ResultError:  errors.New("unsupported type"),
	},
}

func TestReadEnv(t *testing.T) {
	for _, test := range envTestCases {
		t.Run(test.Name, func(t *testing.T) {
			for key, val := range test.Env {
				err := os.Setenv(key, val)
				require.NoError(t, err)
			}

			defer os.Clearenv()

			err := envconfig.ReadEnv(test.ResultConfig, "TEST")
			if test.ResultError != nil {
				require.Error(t, err)
				require.ErrorContains(t, err, test.ResultError.Error())
			} else {
				require.NoError(t, err)
				require.Equal(t, test.WantConfig, test.ResultConfig)
			}
		})
	}
}
