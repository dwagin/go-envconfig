package envconfig

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"reflect"

	"gopkg.in/yaml.v3"
)

// ReadConfig reads configuration file into the structure.
func ReadConfig(config any, fileName string) error {
	var (
		err  error
		file *os.File
	)

	if file, err = os.Open(fileName); err != nil {
		return fmt.Errorf("open file error: %v", err)
	}

	dec := yaml.NewDecoder(bufio.NewReader(file))
	dec.KnownFields(true)

	if err = dec.Decode(config); err != nil && !errors.Is(err, io.EOF) {
		return fmt.Errorf("yaml decode error: %v", err)
	}

	return nil
}

// ReadEnv reads environment variables into the structure.
func ReadEnv(config any, prefixes ...string) error {
	v := reflect.ValueOf(config)
	if v.Kind() != reflect.Pointer {
		return errors.New("pointer expected")
	}

	var prefix string
	if len(prefixes) > 0 {
		prefix = prefixes[0]
		if prefix != "" {
			prefix += EnvSeparator
		}
	}

	_, err := parseStruct(prefix, v.Elem())

	return err
}
